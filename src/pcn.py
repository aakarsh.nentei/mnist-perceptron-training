#!/usr/bin/env python
import math
import matplotlib
from matplotlib import pyplot as plt
import seaborn as sns

from pandas import *

plt.style.use('seaborn')

"""

This is a short coding problem. You will use a perceptron
with 785 inputs (including bias input) and 10 outputs to 
learn to classify the handwritten digits in the MNIST 
dataset. 

    http://yann.lecun.com/exdb/mnist/

See the class slides for details of the perceptron 
architecture and perceptron learning algorithms.


Preprocessing: Scale each fo the data values to 
be between 0 and 1. (i.e) divide each value by 255, 
which is the maximum value in the original data). 
This will help keep the weights from getting too 
large.

Training: Train perceptrons with three different
learning rates \nu = 0.001, 0.01, and .1. 

For each learning rate: 

1. Choose small random initial weight w_i \in [-.5, .5], 
Compute teh accurancy on the training and test sets 
for this initial set of weights, to include 
in your plot. (Call this epoch 0)

Recall the bias unit is always set to 1 and the bias weight
is treated like any other weight.

2. Repeat: Cycle through the training data, changing the 
weights (according to the perceptron learning rule) after
processing each training exampel x_k as follows: 


    - Compute w . x_i at each output unit. 
    - The unit witht the highest value of w . x_i is the 
    prediction for this training example.
    - If there is a correct prediction, don't change 
    the weights and go to the next training example.
    - Otherwise: if there is an incorrect training 
    prediction, then update all the weights in the
    perceptron
        w_i <- w_i + \nu(t^{i} - y^{i})x_i^{i}

        t^{(i)} = 1 if the output unit is correct 0 otherwise.
        and
        y_{(i)} = 1 if w.x^{i} > 0 

        Thus t^({i}) - y^({i}) can be 1, 0 or -1

3. Repeat until training accuracy from one epoch to the next
is than some small number like .01 or through 70 epochs
(iterations through the training set), whichever comes first.

Report: Your report should be one paragraph description of the 
experiment, including for each learning rate. Please do 
not turn your code in for this problem.

- plot the accuracy (fraction of correct classifications) on the 
training and test set at each epoch(including epoch 0), along 
with comments as to whether you are seeing either oscilations 
or overfitting. 

- confiusion Matrix on the test set after the 
training has completed.

"""

sns.set_theme()

from mnist import MNIST
import random 
import numpy as np

mndata = MNIST('./data')
images, labels = mndata.load_training()

# training images - load all the training images and the training data.
training_images, training_labels = mndata.load_training()
scaled_training_images = np.multiply(1/255,  training_images)

test_images, test_labels = mndata.load_testing()
scaled_test_images = np.multiply(1/255,  test_images)


# Each training image is an array of 28*28 inputs. 
canonical_image_idx=random.randrange(0, len(training_images))  # choose an index ;-)
print(training_images[canonical_image_idx])

print("length of an image is: 28x28 image of 784 pixel intensities from 0 to 255", len(training_images[canonical_image_idx]))
print("training Label", training_labels[canonical_image_idx])
print("we can print the training image on the console using the mnist library as follows:")
print(mndata.display(training_images[canonical_image_idx]))

MAX_EPOCHS=70
BIAS_INPUT=1
LEARNING_RATE=0.5
NUMBER_INPUTS = 784
NUMBER_OF_PERCEPTRONS = 10
DEBUG=True
LEARNING_RATES=(0.001, 0.01, 0.1)
MIN_ACCURACY_CHANGE = .000001

def generate_weights_with_bias(): return np.random.uniform(low=-0.5, high=0.5, size=(NUMBER_INPUTS+1,))
 

def one_hot_encoding(n): return np.array([ 1 if n == i  else 0 for i in range(10) ])

def compute_accuracy(images, labels, perceptrons):
    """
    Compute the accuracy on the test set
    """
    correct_predictions = 0
    incorrect_predictions = 0
    #
    confusion_matrix = np.zeros((10, 10))
    for (input_image, label) in zip(images, labels):
        scaled_input_image_with_bias = np.append([1], input_image)
        computed_output = np.matmul(perceptrons, scaled_input_image_with_bias)
        output_label = np.argmax(computed_output)
        confusion_matrix[label][output_label]+=1
        if output_label == label: correct_predictions+=1
        else: incorrect_predictions+=1

    accuracy = correct_predictions/(correct_predictions+incorrect_predictions)
    return (correct_predictions, incorrect_predictions, accuracy, confusion_matrix)
  
"""
    Accuracy < 70 epochs
"""
training_set_recorded_accuracy = np.zeros((len(LEARNING_RATES),MAX_EPOCHS))
test_set_recorded_accuracy     = np.zeros((len(LEARNING_RATES),MAX_EPOCHS))

learning_rate_iteration = 0

for learning_rate in LEARNING_RATES:
    perceptrons = np.array([generate_weights_with_bias() for i in range(NUMBER_OF_PERCEPTRONS)])
    previous_statuc_training_accuracy = 0
    for epoch in range(MAX_EPOCHS):
        print("START EPOCH", epoch)
        """report predictions before training""" 
        test_correct_predictions, test_incorrect_predictions, test_accuracy, test_confusion = compute_accuracy(scaled_test_images, test_labels, perceptrons)
        print("EPOCH", epoch,"learning_rate", learning_rate, "current test accuracy", test_accuracy)
        print("EPOCH ", epoch, "Test confusion matrix\n", DataFrame(test_confusion))
        test_set_recorded_accuracy[learning_rate_iteration][epoch] = test_accuracy
        """Compute Static Training Accuraccy"""
        training_correct_predictions, trainng_incorrect_predictions, static_training_accuracy, training_confusion = compute_accuracy(scaled_training_images, training_labels, perceptrons)
        print("EPOCH", epoch, "learning_rate", learning_rate, "current training accuracy", static_training_accuracy)
        print("EPOCH ", epoch, "Traiing confusion matrix\n", DataFrame(training_confusion))
        training_set_recorded_accuracy[learning_rate_iteration][epoch] = static_training_accuracy

        accuracy_change = abs(static_training_accuracy -  previous_statuc_training_accuracy)
        if accuracy_change < MIN_ACCURACY_CHANGE and epoch !=0 : 
            print("EPOCH", epoch, "accuracy change insignificant", accuracy_change, "current training accuracy", static_training_accuracy)
            #break
        else:
            print("EPOCH", epoch, "accuracy change", accuracy_change, "current training accuracy", static_training_accuracy)

        """ Recored previous statuc training accuracy """
        previous_statuc_training_accuracy = static_training_accuracy

        """Train the perceptrons.""" 
        for (input_image, label) in zip(scaled_training_images, training_labels):
            scaled_input_image_with_bias = np.append([1], input_image)
            computed_output = np.matmul(perceptrons, scaled_input_image_with_bias)

            output_label = np.argmax(computed_output)
            if output_label == label: continue

            for perceptron_idx in range(NUMBER_OF_PERCEPTRONS):
                true_output = 1 if perceptron_idx == label else 0
                logit_output = 1 if computed_output[perceptron_idx] > 0 else 0
                logit_delta = true_output - logit_output
                weight_updates = (learning_rate * logit_delta) * scaled_input_image_with_bias
                perceptrons[perceptron_idx] = np.add(perceptrons[perceptron_idx], weight_updates)

    """print confusion matrix"""
    print("Learning Completed","Learning Rate Used", learning_rate, "Epochs Run", epoch)
    figure, (accuracy_ax, confidence_ax) = plt.subplots(nrows=2, ncols=1) #figsize=(9, 6))


    accuracy_ax.plot(range(MAX_EPOCHS),test_set_recorded_accuracy[learning_rate_iteration], color='green', marker='o', linestyle='dashed',linewidth=0.5, markersize=2, label='Test Set Accuracy')
    accuracy_ax.plot(range(MAX_EPOCHS),training_set_recorded_accuracy[learning_rate_iteration], color='red', marker='x', linestyle='dashed',linewidth=0.5, markersize=2, label='Training Set Accuracy')

    accuracy_ax.legend()
    accuracy_ax.set_xlabel('Epoch') 
    accuracy_ax.set_ylabel('Accuracy')
    accuracy_ax.set_yticks(np.arange(0, 100, 0.5))
    accuracy_ax.margins(.15)
    accuracy_ax.set_title('Accuracy for Learning rate: '+ str(learning_rate), pad=25)

    test_confusion_df = DataFrame(test_confusion)
    sns.heatmap(test_confusion_df, annot=True, fmt='g', cmap='Blues', linewidths=.5, ax=confidence_ax)
    confidence_ax.margins(.15)
    confidence_ax.set_title('Confidence Map for Learning rate: '+str(learning_rate), pad=25)


    figure.tight_layout()

    plt.savefig('mnist-perceptron-training-learning-rate-'+str(learning_rate)+'.png')
    plt.cla()
    learning_rate_iteration+=1
